import scipy as sp
from scipy.special import expit

x = sp.arange(100)
y = sp.arange(100)

# Hyperparameters
# max_breaks = argmax(T)
max_breaks = len(x) - 1
len_th = 1 + (2 + 1) * max_breaks
theta = sp.float_(sp.hstack((max_breaks, sp.arange(2 * max_breaks), x[1:])))
# Gaussian noise variance
sigma = 1
# sigmoid slope for indicator fns approximation
expit_k = 10
# Poisson rate for T prior
P_lambda = 20


def f(x, theta):
    T = sp.int_(theta[0])
    ws = theta[1: 2 * max_breaks + 1]
    ws = ws.reshape((2, max_breaks))
    bs = sp.hstack((0, theta[2 * max_breaks + 1:]))

    x_tilde = sp.column_stack((x, sp.ones(len(x))))
    y = sp.zeros(len(x))
    for t in range(T):
        ind1 = expit(-expit_k * (x - bs[t]))
        ind2 = expit(expit_k * (x - bs[t + 1]))
        z = ind1 * ind2
        y += x_tilde @ ws[:, t] * z
    return y


def loss_fn(theta, x, y):
    T = theta[0]
    T_neigh = sp.int_([sp.floor(T), sp.ceil(T)])
    print(T, T_neigh)

    Ls = sp.zeros(len(T_neigh))
    for i, th in enumerate(T_neigh):
        res = f(x, sp.hstack((th, theta[1:]))) - y
        Ls[i] = 1 / (2 * sigma ** 2) * (res @ res) \
            - th * sp.log(P_lambda) + sp.sum(sp.log(sp.arange(1, th)))
    L = (Ls[1] - Ls[0]) * (T - T_neigh[0]) + Ls[0]
    print(T, Ls)
    return L


Ts = [2, 2.5, 3]
for t in Ts:
    theta[0] = t
    print(t, loss_fn(theta, x, y))
